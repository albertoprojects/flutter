import 'package:meta/meta.dart';

@immutable
class AuthState {
  final String token;

  AuthState({
    @required this.token
  });

  factory AuthState.initial() {
    return new AuthState(token: null);
  }

  AuthState copyWith({String token}) {
    return new AuthState( token: token ?? this.token);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is AuthState &&
              runtimeType == other.runtimeType &&
              token == other.token;

  @override
  int get hashCode => token.hashCode;
}