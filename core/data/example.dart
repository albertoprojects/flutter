import 'package:taskstimer/core/data/dataModel.dart';

class Example extends DataModel {
  String a;
  String b;
  loadData(Map<String, dynamic> data) {
    this.a = data['a'];
    this.b = data['b'];
  }
  toString(){
    return 'id: $id    a: $a    b: $b';
  }
}