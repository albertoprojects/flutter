import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:taskstimer/core/data/firebase/firebaseData.dart';
import 'package:taskstimer/core/data/dataModel.dart';
import 'package:taskstimer/core/data/example.dart';

class DataModelFactory {
  static DataModel getModel<T extends DataModel>() {
    if (T == Example) {
      return new Example();
    }
    return null;
  }
}
class Data {
   static Future<List<T>> getAll<T extends DataModel>(String collectionName) async {
     List<DocumentSnapshot> documentsList = await FirebaseData.getCollectionDocuments(collectionName);
      List<T> modelList =  documentsList.map((document)  {
        return getDocumentData<T>(document);
      }).toList();
      return modelList;
   }

   static Future<T> getDocument<T extends DataModel>(String collectionName, String documentId) async {
     DocumentSnapshot documentSnapshot = await FirebaseData.getCollectionDocument(collectionName, documentId);
     return getDocumentData<T>(documentSnapshot);
   }

   static T getDocumentData<T extends DataModel>(DocumentSnapshot documentSnapshot) {
    DataModelFactory.getModel<T>();
    T modelElement = DataModelFactory.getModel<T>();
    modelElement.setId(documentSnapshot.documentID);
    modelElement.loadData(documentSnapshot.data);
    return modelElement;
   }
}