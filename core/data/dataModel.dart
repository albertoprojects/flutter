import 'package:flutter/material.dart';
abstract class DataModel {
  @protected
  String id;
  setId(String id ){
    this.id = id;
  }
  loadData(Map<String, dynamic> data);
}