import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseData {
  static Future<List<DocumentSnapshot>> getCollectionDocuments(String collectionName) async {
    QuerySnapshot  collectionDocuments =  await Firestore.instance.collection(collectionName).reference().getDocuments();
    return collectionDocuments.documents;
  }
  static Future<DocumentSnapshot> getCollectionDocument(String collectionName, String documentId) async {
    DocumentSnapshot  documentSnapshot =  await Firestore.instance.collection(collectionName).document(documentId).get();
    return documentSnapshot;
  }
}