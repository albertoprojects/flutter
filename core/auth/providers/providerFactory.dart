import 'package:taskstimer/core/auth/providers/baseProvider.dart';
import 'package:taskstimer/core/auth/providers/facebookAuth.dart';
import 'package:taskstimer/core/auth/providers/googleAuth.dart';

class ProviderFactory {
  static BaseProvider getAuthType<T extends BaseProvider>() {
    if (T == FacebookAuth) {
      return new FacebookAuth();
    }
    if (T == GoogleAuth) {
      return new GoogleAuth();
    }
    return null;
  }
}
