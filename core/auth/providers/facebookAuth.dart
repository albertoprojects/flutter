import 'package:taskstimer/core/auth/providers/baseProvider.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:taskstimer/core/auth/exceptions.dart';

class FacebookAuth extends BaseProvider {
  FacebookLogin _facebookLogin;
  FacebookLoginResult _result;

  signIn() async {
    _facebookLogin = FacebookLogin();
    _result = await _facebookLogin.logInWithReadPermissions(['email']);
    switch (_result.status) {
      case FacebookLoginStatus.loggedIn:
        break;
      case FacebookLoginStatus.cancelledByUser:
        throw new CancelledByUser();
        break;
      case FacebookLoginStatus.error:
        throw new SingInError();
        break;
    }
  }

  signOut() async {
    try {
      await _facebookLogin.logOut();
    } on Exception {
      throw new SignOutError();
    }
    
  }

  Future<Map<String, dynamic>> getCredentialsData() async {
    try{
      Map<String, dynamic> credentialsMap = new Map<String, dynamic>();
      credentialsMap['accessToken'] = _result.accessToken.token;
      return credentialsMap;
    } on Exception {
      throw new GetCredentialsError();
    }
    
  }

  isSigned() {
    return (_result != null &&  _result.accessToken != null);
  }
}
