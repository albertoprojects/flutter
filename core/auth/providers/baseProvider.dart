abstract class BaseProvider {
  signIn();
  signOut();
  isSigned();
  Future<Map<String, dynamic>> getCredentialsData();
}
