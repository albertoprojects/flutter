import 'package:taskstimer/core/auth/providers/baseProvider.dart';
import 'package:taskstimer/core/auth/exceptions.dart';

class NoAuthBaseProvided implements Exception {
 
}

class SecuredAuth<T extends BaseProvider> implements BaseProvider {
  
  T _authBaseProvider;

  SecuredAuth(T authBaseProvider) {
    if (authBaseProvider == null) {
      throw new NoAuthBaseProvided();
    }
    _authBaseProvider = authBaseProvider;
  }
  signIn() async {
    if (isSigned()) {
      throw new AlreadySigned();
    }
 
    await _authBaseProvider.signIn();

    
  }
  signOut() async {
    if (!isSigned()) {
      throw new NoSigned();
    }

    await _authBaseProvider.signOut();

  }
  getCredentialsData() async{
    if (!isSigned()) {
      throw new NoSigned();
    }
    return await _authBaseProvider.getCredentialsData();
  }

  isSigned() {
    return _authBaseProvider.isSigned();
  }
}