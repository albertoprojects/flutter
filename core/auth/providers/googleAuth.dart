import 'package:flutter/services.dart';
import 'package:taskstimer/core/auth/providers/baseProvider.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:taskstimer/core/auth/exceptions.dart';

class GoogleAuth implements BaseProvider {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  GoogleSignInAccount _googleAccount;

  signIn() async {
    try {
       _googleAccount = await _googleSignIn.signIn();
    }on PlatformException catch(e) {
      switch (e.code) {
        case 'ERROR_USER_DISABLED':
        case 'ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL':
        case 'ERROR_INVALID_CREDENTIAL':
        case 'ERROR_OPERATION_NOT_ALLOWED':
        default:
          throw new SingInError();
      }
    }
   
    if (_googleAccount == null ){
      throw new CancelledByUser();
    }
  }

  signOut() async {
    try {
      _googleAccount = await _googleSignIn.signOut();
      _googleAccount = null;
    }on Exception {
      throw new SignOutError();
    }
  }

  Future<Map<String, dynamic>> getCredentialsData() async {
    try {
      GoogleSignInAuthentication googleAuth =
          await _googleAccount.authentication;
          final Map<String, dynamic> credentialsMap = {
            "accessToken": googleAuth.accessToken,
            "idToken": googleAuth.idToken
          };
          return credentialsMap;
    } on Exception {
      throw new GetCredentialsError();
    }  
  }


  isSigned() {
    return (_googleAccount != null);
  }
}
