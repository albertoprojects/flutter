import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
abstract class AuthBase {
  @protected
  FirebaseUser user;
  signIn([Map<String, dynamic> signInInfo]);
  signOut();
  isSigned();
  FirebaseUser getUser() {
    return user;
  }
}
