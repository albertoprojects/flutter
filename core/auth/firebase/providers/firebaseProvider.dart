import 'package:firebase_auth/firebase_auth.dart';
import 'package:taskstimer/core/auth/firebase/authBase.dart';
import 'package:taskstimer/core/auth/providers/baseProvider.dart';
import 'package:taskstimer/core/auth/providers/providerFactory.dart';
import 'package:taskstimer/core/auth/providers/securedAuth.dart';


abstract class FirebaseProvider<T extends BaseProvider> extends AuthBase {
  BaseProvider _authBaseProvider;
  FirebaseAuth _fAuth;
  FirebaseProvider() {
    _fAuth = FirebaseAuth.instance;
    _authBaseProvider = ProviderFactory.getAuthType<T>();
  }

  signIn([Map<String, dynamic> signInInfo]) async{
    await _authBaseProvider.signIn();
    AuthCredential credentials = await this.getAuthCredentials();
    user = await _fAuth.signInWithCredential(credentials);
  }
  signOut() async {
    await _fAuth.signOut();
    await _authBaseProvider.signOut();
  }

  isSigned() async {
    await _authBaseProvider.isSigned();
  }

  Future<AuthCredential> getAuthCredentials() async {
    Map<String, dynamic> credentialsData =
        await _authBaseProvider.getCredentialsData();
    return this.createAuthCredentials(credentialsData);
  }

  AuthCredential createAuthCredentials(
      Map<String, dynamic> credentialsData);

}
