import 'package:firebase_auth/firebase_auth.dart';
import 'package:taskstimer/core/auth/providers/googleAuth.dart';
import 'package:taskstimer/core/auth/firebase/providers/firebaseProvider.dart';

class GoogleFirebaseAuth extends FirebaseProvider<GoogleAuth> {
  AuthCredential createAuthCredentials(Map<String, dynamic> credentialsData) {
    final AuthCredential credentials = GoogleAuthProvider.getCredential(
      accessToken: credentialsData['accessToken'],
      idToken: credentialsData['idToken'],
    );
    return credentials;
  }
}
