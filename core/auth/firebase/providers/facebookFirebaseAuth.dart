import 'package:firebase_auth/firebase_auth.dart';
import 'package:taskstimer/core/auth/providers/facebookAuth.dart';
import 'package:taskstimer/core/auth/firebase/providers/firebaseProvider.dart';

class FacebookFirebaseAuth extends FirebaseProvider<FacebookAuth> {
  AuthCredential createAuthCredentials(Map<String, dynamic> credentialsData) {
    final AuthCredential credentials = FacebookAuthProvider.getCredential(
      accessToken: credentialsData['accessToken']
    );
    return credentials;
  }

}
