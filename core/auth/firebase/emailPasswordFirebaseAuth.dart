import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:taskstimer/core/auth/exceptions.dart';
import 'package:taskstimer/core/auth/firebase/authBase.dart';

class EmailPasswordFirebaseAuth extends AuthBase {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  signIn([Map<String, dynamic> signInInfo]) async {
    user = await _auth.signInWithEmailAndPassword(email: signInInfo['email'], password: signInInfo['password']);
  }

  signOut() async {
      _auth.signOut();
      user = null;
  }

  isSigned() {
    return (user != null);
  }
}
