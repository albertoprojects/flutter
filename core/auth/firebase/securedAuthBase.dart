import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:taskstimer/core/auth/exceptions.dart';
import 'package:taskstimer/core/auth/firebase/authBase.dart';

class NoFirebaseAuthProvided implements Exception {
 
}


class SecuredAuthBase<T extends AuthBase> extends AuthBase {
  T _firebaseAuth;

  SecuredAuthBase(T firebaseAuth ){
    if (firebaseAuth == null) {
      throw new NoFirebaseAuthProvided();
    }
    _firebaseAuth = firebaseAuth;
  }
  signIn([Map<String, dynamic> signInInfo]) {
    try {
      this._firebaseAuth.signIn(signInInfo);
    }on PlatformException{
      throw new SingInError();
    }
    if (this._firebaseAuth.getUser() == null) {
      throw new SingInError();
    }
  }
  signOut() {
    try {
      this._firebaseAuth.signOut();
    } on Exception {
      throw new SignOutError();
    }
  }
  isSigned() {
    return this._firebaseAuth.isSigned();
  }
}