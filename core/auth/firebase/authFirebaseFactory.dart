import 'package:taskstimer/core/auth/firebase/authBase.dart';
import 'package:taskstimer/core/auth/firebase/securedAuthBase.dart';
import 'package:taskstimer/core/auth/firebase/emailPasswordFirebaseAuth.dart';
import 'package:taskstimer/core/auth/firebase/providers/facebookFirebaseAuth.dart';
import 'package:taskstimer/core/auth/firebase/providers/googleFirebaseAuth.dart';

class AuthFirebaseFactory {
  static AuthBase getAuthType<T extends AuthBase>() {
    if (T == FacebookFirebaseAuth) {
      return new SecuredAuthBase<FacebookFirebaseAuth>(new FacebookFirebaseAuth());
    }
    if (T == GoogleFirebaseAuth) {
      return  new SecuredAuthBase<GoogleFirebaseAuth>(new GoogleFirebaseAuth());
    }
    if (T == EmailPasswordFirebaseAuth ) {
      return new SecuredAuthBase<EmailPasswordFirebaseAuth>(new EmailPasswordFirebaseAuth());
    }
    return null;
  }
}
